.. include:: /defs.hrst

.. _python_api:

==========
Python API
==========

.. toctree::
    :maxdepth: 2

    tables
    tracebacks
